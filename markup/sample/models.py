from django.db import models as m
from markupfield.fields import MarkupField


class Entry(m.Model):
    name = m.CharField(max_length=80)
    text = m.TextField()
    markup = MarkupField()
