from django.conf.urls.defaults import patterns, include, url
from django.views.generic import DetailView, CreateView
from sample.models import Entry
from django import forms

from django.contrib import admin
admin.autodiscover()


class EntryForm(forms.ModelForm):
    class Meta:
        model = Entry


class EntryCreateView(CreateView):
    form_class = EntryForm
    template_name = 'sample/entry_new.html'
    success_url = '/admin'


urlpatterns = patterns(
    '',
    (r'^entry/(?P<pk>\d+)/$',
        DetailView.as_view(model=Entry)),

    (r'^entry/create/$',
        EntryCreateView.as_view()),

    url(r'^admin/', include(admin.site.urls)),
)
